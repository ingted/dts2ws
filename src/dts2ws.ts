// This code is licenced under the GNU GPLv3
// (c) David Koňařík 2020
import * as ts from "typescript";
import arg from "arg";
import { off } from "process";
import { assert } from "console";

interface DocEntry {
    name?: string;
    fileName?: string;
    documentation?: string;
    type?: string;
    constructors?: DocEntry[];
    parameters?: DocEntry[];
    returnType?: string;
}

type Type =
    | SimpleType
    | GenericType
    | OptionalType
    | UnionType
    | FunctionType

interface TypeBase {
    kind: "simple" | "generic" | "optional" | "union" | "function";
}

interface SimpleType extends TypeBase {
    name: string;
}

interface GenericType extends TypeBase {
    name: string;
    params: Type[];
}

interface OptionalType extends TypeBase {
    inner: Type;
}

interface UnionType extends TypeBase {
    inners: Type[];
}

interface FunctionType extends TypeBase {
    params: {name: string, type: Type}[];
    returnType: Type;
}

type MemberKind =
    | "method"
    | "property"
    | "constructor"

interface Method {
    kind: MemberKind;
    name: string;
    returnType: Type;
    params: {name: string, type: Type}[];
}

interface Property {
    kind: MemberKind;
    name: string;
    type: Type;
}

interface Constructor {
    kind: MemberKind;
    params: {name: string, type: Type}[];
}

type Construct =
    | Class
    | Interface
    | FreeFunctions

type ConstructKind =
    | "class"
    | "interface"
    | "freeFuncs"

interface Class {
    kind: ConstructKind;
    nsPath: string[];
    members: (Method | Property | Constructor)[];
    extended?: Type;
    implemented: Type[];
}

interface Interface {
    kind: ConstructKind;
    nsPath: string[];
    members: (Method | Property)[];
    implemented: Type[];
}

interface Function {
    name: string;
    returnType: Type;
    params: {name: string, type: Type}[];
}

interface FreeFunctions {
    kind: ConstructKind;
    nsPath: string[];
    functions: Function[];
}

function stringyUniq<T>(items: T[]): T[] {
    const set =
        new Set(items.map(x => JSON.stringify(x)));
    return Array.from(set).map(s => JSON.parse(s));
}

function processType(tc: ts.TypeChecker, t: ts.Type): Type {
    const tr = <ts.TypeReference>t;
    // TODO: How to get names of some types without this ugly cast?
    const tAny = <any>t;

    function getName() {
        const sym = t.getSymbol();
        if(sym) {
            return tc.getFullyQualifiedName(sym);
        } else {
            return tAny.intrinsicName || "";
        }
    }

    // TODO: Support unions of literals as enums
    if(t.isNumberLiteral()) {
        return { kind: "simple", name: "number" };
    } else if(t.isStringLiteral()) {
        return { kind: "simple", name: "string" };
    } else if(t.flags & ts.TypeFlags.BooleanLike) {
        return { kind: "simple", name: "boolean" }
    // Bools are internally unions in TypeScript, neat
    } else if(t.isUnion() && tAny.intrinsicName != "boolean") {
        const uit = <ts.UnionOrIntersectionType>t;
        const inners = stringyUniq(uit.types.map(t => processType(tc, t)));
        // This is a roundabout way to delete duplicates
        if(inners.length == 1) {
            return inners[0];
        } else {
            return { kind: "union", inners: inners };
        }
    } else if(tr.typeArguments && tr.typeArguments.length > 0) {
        return  {
            kind: "generic",
            name: getName(),
            params: tr.typeArguments?.map(ta => processType(tc, ta)) || [],
        };
    } else {
        return  {
            kind: "simple",
            name: getName(),
        };
    }
}

function processTypeNode(tc: ts.TypeChecker, n?: ts.TypeNode): Type {
    if(!n) {
        console.warn("Type is missing, presuming void");
        return <SimpleType> {
            kind: "simple",
            name: "void",
        };
    }

    // It seems you can't get function type data from ts.Type, only from
    // ts.TypeNode, which is why we need all this...
    if(ts.isParenthesizedTypeNode(n)) {
        return processTypeNode(tc, n.type);
    } else if(ts.isArrayTypeNode(n)) {
        const atn = <ts.ArrayTypeNode>n;
        return <GenericType> {
            kind: "generic",
            name: "Array",
            params: [processTypeNode(tc, atn.elementType)],
        };
    } else if(ts.isUnionTypeNode(n)) {
        const utn = <ts.UnionTypeNode>n;
        const inners = stringyUniq(utn.types.map(t => processTypeNode(tc, t)));
        return <UnionType> { kind: "union", inners };
    } else if(ts.isFunctionTypeNode(n)) {
        const ftn = <ts.FunctionTypeNode>n;
        return <FunctionType> {
            kind: "function",
            params: ftn.parameters.map(p => ({
                name: p.name.getText(),
                type: processTypeNode(tc, p.type),
            })),
            returnType: processTypeNode(tc, ftn.type),
        };
    }

    return processType(tc, tc.getTypeFromTypeNode(n));
}

function processTypeOfNode(
        tc: ts.TypeChecker,
        n: {questionToken?: ts.QuestionToken, type?: ts.TypeNode}): Type {
    let type = processTypeNode(tc, n.type);
    if(n.questionToken) {
        type = <OptionalType> { kind: "optional", inner: type };
    }
    return type;
}

function processParam(tc: ts.TypeChecker, p: ts.ParameterDeclaration) {
    return {
        name: p.name.getText()!,
        type: processTypeOfNode(tc, p),
    };
}

function visitClass(
        tc: ts.TypeChecker, cls: ts.ClassDeclaration,
        nsPath: string[]): Class {
    const members: (Method | Property | Constructor)[] = [];
    let extended: Type | undefined;
    const implemented: Type[] = [];
    cls.forEachChild(n => {
        if(ts.isMethodDeclaration(n)) {
            const m = <ts.MethodDeclaration>n;
            members.push({
                kind: "method",
                name: m.name.getText(),
                returnType: processTypeOfNode(tc, m),
                params: m.parameters.map(p => processParam(tc, p)),
            });
        } else if(ts.isPropertyDeclaration(n)) {
            const p = <ts.PropertyDeclaration>n;
            let type = processTypeOfNode(tc, p);
            if(p.questionToken) {
                type = { kind: "optional", inner: type };
            }
            members.push({
                kind: "property",
                name: p.name.getText(),
                type: type,
            });
        } else if(ts.isConstructorDeclaration(n)) {
            const c = <ts.ConstructorDeclaration>n;
            members.push({
                kind: "constructor",
                params: c.parameters.map(p => processParam(tc, p)),
            });
        } else if(ts.isHeritageClause(n)) {
            const hc = <ts.HeritageClause>n;
            if(hc.token == ts.SyntaxKind.ExtendsKeyword) {
                extended =
                    processType(tc,
                        tc.getTypeAtLocation(hc.types[0].expression));
            } else {
                for(const t of hc.types) {
                    implemented.push(
                        processType(tc, tc.getTypeAtLocation(t.expression)));
                }
            }
        }
    });
    return {
        kind: "class",
        nsPath: [...nsPath, cls.name!.getText()],
        members: members,
        extended, implemented,
    };
}

function visitInterface(
        tc: ts.TypeChecker, iface: ts.InterfaceDeclaration,
        nsPath: string[]): Interface {
    const members: (Method | Property)[] = [];
    const implemented: Type[] = [];
    iface.forEachChild(n => {
        if(ts.isMethodSignature(n)) {
            const m = <ts.MethodSignature>n;
            members.push({
                kind: "method",
                name: m.name.getText(),
                returnType: processTypeOfNode(tc, m),
                params: m.parameters.map(p => processParam(tc, p)),
            });
        } else if(ts.isPropertySignature(n)) {
            const p = <ts.PropertySignature>n;
            members.push({
                kind: "property",
                name: p.name.getText(),
                type: processTypeOfNode(tc, p),
            });
        } else if(ts.isHeritageClause(n)) {
            const hc = <ts.HeritageClause>n;
            if(hc.token != ts.SyntaxKind.ExtendsKeyword) {
                console.error("Heritage clause of interface not Extends!", iface.name.getText());
                return;
            }
            for(const t of hc.types) {
                implemented.push(
                    processType(tc, tc.getTypeAtLocation(t.expression)));
            }
        }
    });
    return {
        kind: "interface",
        nsPath: [...nsPath, iface.name!.getText()],
        members: members,
        implemented,
    };
}

function visitFunction(
        tc: ts.TypeChecker, fun: ts.FunctionDeclaration,
        nsPath: string[]): Function {
    return {
        name: fun.name?.getText() || "",
        params: fun.parameters.map(p => processParam(tc, p)),
        returnType: processTypeOfNode(tc, fun),
    };
}

function visitTopLevel(tc: ts.TypeChecker, node: ts.Node, nsPath: string[]) {
    const constructs: (Class | Interface | FreeFunctions)[] = [];
    if(ts.isModuleDeclaration(node)) {
        const mod = <ts.ModuleDeclaration>node
        const modName = mod.name.getText();
        node.forEachChild(n => {
            const cs = visitTopLevel(tc, n, [...nsPath, modName]);
            constructs.push(...cs);
        });
    } else if(ts.isModuleBlock(node)) {
        const functions: Function[] = [];
        node.forEachChild(n => {
            const cs = visitTopLevel(tc, n, nsPath);
            constructs.push(...cs);

            if(ts.isFunctionDeclaration(n)) {
                const fun = <ts.FunctionDeclaration>n;
                functions.push(visitFunction(tc, fun, nsPath));
            }
        });

        if(functions.length > 0) {
            constructs.push({kind: "freeFuncs", nsPath, functions});
        }
    } else if(ts.isClassDeclaration(node)) {
        const cls = <ts.ClassDeclaration>node;
        constructs.push(visitClass(tc, cls, nsPath));
    } else if(ts.isInterfaceDeclaration(node)) {
        const iface = <ts.InterfaceDeclaration>node;
        constructs.push(visitInterface(tc, iface, nsPath));
    }
    return constructs;
}

function wigEscapeIdent(ident: string) {
    const keywords = [
        "abstract", "and", "as", "assert", "base", "begin", "class",
        "default", "delegate", "do", "done", "downcast", "downto", "elif",
        "else", "end", "exception", "extern", "false", "finally", "fixed",
        "for", "fun", "function", "global", "if", "in", "inherit", "inline",
        "interface", "internal", "lazy", "let", "let!", "match", "match!",
        "member", "module", "mutable", "namespace", "new", "not", "null",
        "of", "open", "or", "override", "private", "public", "rec", "return",
        "return!", "select", "static", "struct", "then", "to", "true", "try",
        "type", "upcast", "use", "use!", "val", "void", "when", "while",
        "with", "yield", "yield!", "const", "asr", "land", "lor", "lsl",
        "lsr", "lxor", "mod", "sig", "atomic", "break", "checked",
        "component", "const", "constraint", "constructor", "continue",
        "eager", "event", "external", "functor", "include", "method",
        "mixin", "object", "parallel", "process", "protected", "pure",
        "sealed", "tailcall", "trait", "virtual", "volatile"];
    if(keywords.includes(ident)) {
        return "``" + ident + "``";
    } else {
        return ident;
    }
}

function getWigName(nsPath: string[] | string, stripNss: string[]): string {
    if(nsPath instanceof Array) {
        return getWigName(nsPath.join("."), stripNss);
    }

    for(const ns of stripNss) {
        if(nsPath.startsWith(ns + ".")) {
            nsPath = nsPath.slice(ns.length + 1);
            break;
        }
    }

    return wigEscapeIdent(nsPath.replace(/\./g, "_"));
}

function wigType(t: Type, stripNss: string[], selfName: string): string {
    if(t.kind == "simple") {
        const st = <SimpleType>t;
        if(st.name == "string") {
            return "T<string>";
        } else if(st.name == "number") {
            return "T<float>";
        } else if(st.name == "boolean") {
            return "T<bool>";
        } else if(st.name == "any" || st.name == "object"
                  || st.name == "Object") {
            return "T<obj>";
        } else if(st.name == "void") {
            return "T<unit>";
        } else if(st.name == "Function") {
            return "(T<unit> ^-> T<unit>)";
        } else if(st.name == "__type") {
            console.warn("Encountered undefined type!");
            return "T<obj>";
        // This is a non-exhaustive list of browser-standard types that should
        // translate to a WebSharper version
        } else if(/HTML.*Element/.test(st.name)) {
            return "T<HTMLElement>";
        } else if(st.name == "Date") {
            return "T<Date>";
        } else if(st.name == selfName) {
            return "TSelf";
        } else {
            return getWigName(st.name, stripNss);
        }
    } else if(t.kind == "generic") {
        const gt = <GenericType>t;
        if(gt.name == "Array") {
            return "(!| " + wigType(gt.params[0], stripNss, selfName) + ")";
        } else {
            console.error("Arbitrary generics aren't supported yet!");
            return "TODO-generic"
        }
    } else if(t.kind == "optional") {
        const ot = <OptionalType>t;
        return "(!? " + wigType(ot.inner, stripNss, selfName) + ")";
    } else if(t.kind == "union") {
        const ut = <UnionType>t;
        return "(" + ut.inners.map(i =>
            wigType(i, stripNss, selfName)).join(" + ") + ")";
    } else if(t.kind == "function") {
        const ft = <FunctionType>t;
        let params =
            ft.params.map(p =>
                wigType(p.type, stripNss, selfName) + "?" + wigEscapeIdent(p.name))
            .join(" * ");
        if(params == "") {
            params = "T<unit>";
        }
        return "(" + params + " ^-> "
            + wigType(ft.returnType, stripNss, selfName) + ")";
    }
    return "UNREACHABLE";
}

function wigMember(
        m: Constructor | Method | Property, stripNss: string[],
        selfName: string) {
    if(m.kind == "constructor") {
        const ctor = <Constructor>m;
        const params = ctor.params.map(p => {
            const type = wigType(p.type, stripNss, selfName);
            const name = wigEscapeIdent(p.name);
            return `${type}?${name}`
        }).join(" * ");
        return `Constructor (${params})`;
    } else if(m.kind == "method") {
        const method = <Method>m;
        let params = method.params.map(p => {
            const type = wigType(p.type, stripNss, selfName);
            const name = wigEscapeIdent(p.name);
            return `${type}?${name}`
        }).join(" * ");
        if(params == "") {
            params = "T<unit>";
        }
        const ret = wigType(method.returnType, stripNss, selfName);
        return `"${method.name}" => ${params} ^-> ${ret}`;
    } else if(m.kind == "property") {
        const prop = <Property>m;
        return `"${prop.name}" =@ ${wigType(prop.type, stripNss, selfName)}`;
    }
}

function getParentInterfaces(
        constructs: Construct[], iface: Interface): Interface[] {
    return (<Interface[]>[]).concat(...iface.implemented.map(parentRef => {
        assert(parentRef.kind == "simple");
        const parentSt = <SimpleType> parentRef;
        const parent = constructs.find(
            c => c.nsPath.join(".") == parentSt.name);
        if(!parent) {
            console.error(`Parent interface of ${iface.nsPath.join(".")} (${parentSt.name}) not found!`);
            return [];
        }
        assert(parent.kind == "interface");
        const parentIface = <Interface> parent;
        return [parentIface, ...getParentInterfaces(constructs, parentIface)];
    }));
}

function createWig(
        constructs: Construct[], stripNss: string[]) {
    let out = "";
    const freeFuncs =
        constructs
        .filter(c => c.kind == "freeFuncs")
        .map(c => <FreeFunctions>c);
    const classesAndIfaces =
        constructs
        .filter(c => c.kind == "class" || c.kind == "interface");

    out +=
`type Helpers =
    static member NormaliseType(t: Type.Type) = t
    static member NormaliseType(t: CodeModel.Class) = t.Type

`;

    // First forward-declare everything to allow out-of-order references
    for(const c of classesAndIfaces) {
        const wigName = getWigName(c.nsPath, stripNss);

        out += `let ${wigName} = Class "${c.nsPath.join(".")}"\n`;
    }
    out += "\n";

    for(const c of classesAndIfaces) {
        const name = c.nsPath[c.nsPath.length - 1];
        // TODO: Nested classes/namespaces?
        const wigName = getWigName(c.nsPath, stripNss);

        out += wigName + "\n";
        //out += `    |> WithSourceName "${c.nsPath.join(".")}"\n`;
        out += `    |> WithSourceName "${wigName}"\n`;

        let members;
        if(c.kind == "class") {
            const cls = <Class>c;
            members = cls.members;

            if(cls.extended) {
                const t = wigType(cls.extended, stripNss, name);
                out += `    |=> Inherits ${t}\n`;
            }
            if(cls.implemented.length > 0) {
                const types = cls.implemented
                    .map(t => wigType(t, stripNss, name))
                    .join("; ");
                out += `    |=> Implements [${types}]\n`;
            }
        } else {
            const iface = <Interface>c;

            const ancestry = [iface, ...getParentInterfaces(constructs, iface)];
            const allMembers = (<(Property | Method)[]>[]).concat(
                ...ancestry.map(p => p.members));
            let allProps = allMembers
                .filter(m => m.kind == "property")
                .map(p => <Property>p);
            // Deduplicate (why is this needed?)
            allProps = allProps.filter((val, idx, arr) =>
                arr.findIndex(p => p.name == val.name) == idx);

            // We would get collisions with the setter functions if we actually
            // did inheritance, so we don't formally inherit and flatten the
            // members. This has the potential to break a lot of things, but
            // it'll mostly work

            members = [...allProps, ...allMembers.filter(m => m.kind != "property")];

            /*if(iface.implemented.length > 0) {
                // We translate TS interfaces into WIG classes, so we can only
                // inherit one
                if(iface.implemented.length > 1) {
                    console.error(
                        "Interface", name,
                        "inherits multiple times, this won't work!");
                }
                const type = wigType(iface.implemented[0], stripNss, name);
                out += `    |=> Inherits ${type}\n`;
            }*/

            // Unfortunately, due to the fact that each type variant of a function
            // has to be compiled into a separate .NET method, we can't have
            // constructor methods taking too many arguments that are erased unions.
            // (exponential growth). Instead, we'll create chainable Set... methods
            // (TODO: Colisions?) for all types and only create constructors for
            // some.
            // In the current implementation, optional params create multtiple
            // variants as well for some reason instead of default args :(

            const allOptProps = allProps.filter(p => p.type.kind == "optional");
            // TODO: Only count optional/union typed props
            if(allProps.length < 5 && allProps.length > 0
               && allOptProps.length > 0) {
                const fields =
                    allProps
                    .map(p => {
                        const t = wigType(p.type, stripNss, "");
                        const n = wigEscapeIdent(p.name);
                        return `${t}?${n}`;
                    })
                    .join(" * ");

                out += `    |+> Static [ ObjectConstructor (${fields}) ]\n`;
            } else {
                out += `    |+> Static [ Constructor T<unit> |> WithInteropInline (fun _ -> "{}") ]\n`;
                out += "    |+> Instance [\n";
                for(const prop of allProps) {
                    const pName = prop.name[0].toUpperCase() + prop.name.slice(1);
                    let pType;
                    if(prop.type.kind == "optional") {
                        pType = wigType((<OptionalType>prop.type).inner, stripNss, name);
                    } else {
                        pType = wigType(prop.type, stripNss, name);
                    }
                    out += `        "Set${pName}" => ${pType}?v ^-> TSelf\n`;
                    out += `        |> WithInteropInline (fun tr -> "($this.${prop.name} = " + tr "v" + ", $this)")\n`;
                }
                out += "    ]\n";
            }
        }

        function propToPair(p: Property) {
            let t = wigType(p.type, stripNss, name);
            return `"${p.name}", Helpers.NormaliseType(${t})`;
        }

        const props = members
            .filter(m => m.kind == "property")
            .map(p => <Property> p);
        const optProps = props
            .filter(p => p.type.kind == "optional")
        const optPropsStr = optProps
            .map(p =>
                "        "
                + propToPair({
                    kind: "property",
                    name: p.name,
                    type: (<OptionalType>p.type).inner
                })
                + "\n")
            .join("");
        const nonOptProps = props
            .filter(p => p.type.kind != "optional");
        const nonOptPropsStr = nonOptProps
            .map(p => "        " + propToPair(p) + "\n")
            .join("");
        const otherMembers = members
            .filter(m => m.kind != "property")
            .map(m => "        " + wigMember(m, stripNss, name) + "\n")
            .join("");

        if(otherMembers.length > 0) {
            out += `    |+> Instance [\n${otherMembers}    ]\n`;
        }
        if(nonOptProps.length > 0) {
            out += `    |+> Pattern.RequiredFields [\n${nonOptPropsStr}    ]\n`;
        }
        if(optProps.length > 0) {
            out += `    |+> Pattern.OptionalFields [\n${optPropsStr}    ]\n`;
        }

        out += "    |> ignore\n";
        out += "\n";
    }

    for(const ffuns of freeFuncs) {
        out += `let ${getWigName(ffuns.nsPath, stripNss)} =\n`;
        out += `    Class "${ffuns.nsPath.join(".")}"\n`;

        const funcs = ffuns.functions.map(f => {
            let params = f.params.map(p =>
                `${wigType(p.type, stripNss, "")}?${p.name}`).join(" * ");
            if(params == "") {
                params = "T<unit>";
            }
            const ret = wigType(f.returnType, stripNss, "");
            return `        "${f.name}" => ${params} ^-> ${ret}\n`;
        });

        out += `    |+> Static [\n${funcs.join("")}    ]\n`;

    }

    out += "let NamespaceEntities: CodeModel.NamespaceEntity list = [\n";
    for(const c of constructs) {
        const wigName = getWigName(c.nsPath, stripNss);
        out += `    ${wigName}\n`;
    }
    out += "]\n";

    return out;
}

function processFiles(
        fileNames: string[],
        options: ts.CompilerOptions,
        stripNss: string[]): void {
    let program = ts.createProgram(fileNames, options);

    let tc = program.getTypeChecker();
    let output: DocEntry[] = [];

    const constructs: Construct[] = [];
    for (const sourceFile of program.getSourceFiles()) {
        if(!fileNames.includes(sourceFile.fileName)) {
            continue;
        }
        if (!sourceFile.isDeclarationFile) {
            console.warn("Given non-declaration file, skipping...");
            continue;
        }
        ts.forEachChild(sourceFile, (n) => {
            const cs = visitTopLevel(tc, n, []);
            constructs.push(...cs);
        });
    }
    console.log(createWig(constructs, stripNss));
}

const args = arg({
    "--stripNamespace": [String],
});

processFiles(args._, {
        target: ts.ScriptTarget.ES5,
        module: ts.ModuleKind.CommonJS
    }, args["--stripNamespace"] || []);
